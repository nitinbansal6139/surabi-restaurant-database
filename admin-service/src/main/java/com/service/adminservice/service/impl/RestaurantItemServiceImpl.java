package com.service.adminservice.service.impl;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.service.adminservice.entity.FinancialReport;
import com.service.adminservice.repository.FinancialReportRepository;
import com.service.adminservice.service.RestaurantItemService;

@Service
public class RestaurantItemServiceImpl implements RestaurantItemService {

	@Autowired
	EntityManager entityManager;

	@Autowired
	FinancialReportRepository financialReportRepository;

	@Override
	public List<FinancialReport> viewDailyReport() {
		List<FinancialReport> reports = financialReportRepository.findTodaysPurchases();
		return reports;
	}

	@Override
	public List<FinancialReport> totalMonthlySale() {
		LocalDate localDate = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		int year = localDate.getYear();
		int month = localDate.getMonthValue();
		TypedQuery<FinancialReport> purchasesFromThisMonth = entityManager
				.createQuery("Select l from FinancialReport l where extract(month from l.purchaseDate)=" + month
						+ " and extract(year from l.purchaseDate)=" + year, FinancialReport.class);
		return purchasesFromThisMonth.getResultList();
	}

	@Override
	public List<Object[]> getAllSalesByCities() {
		return financialReportRepository.getAllSalesByCities();
	}

	@Override
	public List<Object[]> getMaxSale() {
		return financialReportRepository.getMaxSale();
	}

	@Override
	public List<Object[]> getAllSalesByMonths() {
		return financialReportRepository.getAllSalesByMonths();
	}

}
