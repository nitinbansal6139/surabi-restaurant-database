package com.service.offlineuserservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OfflineuserServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OfflineuserServiceApplication.class, args);
	}

}
