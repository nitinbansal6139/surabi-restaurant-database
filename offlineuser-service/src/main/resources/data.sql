INSERT INTO items (id, name, price)
SELECT * FROM (SELECT '1' AS id, 'Pasta' AS name, '35' AS price) AS tmp
WHERE NOT EXISTS (
    SELECT name FROM items WHERE id = '1'
) LIMIT 1;

INSERT INTO items (id, name, price)
SELECT * FROM (SELECT '2' AS id, 'Pizza' AS name, '85' AS price) AS tmp
WHERE NOT EXISTS (
    SELECT name FROM items WHERE id = '2'
) LIMIT 1;

INSERT INTO items (id, name, price)
SELECT * FROM (SELECT '3' AS id, 'Noodles' AS name, '25' AS price) AS tmp
WHERE NOT EXISTS (
    SELECT name FROM items WHERE id = '3'
) LIMIT 1;

INSERT INTO items (id, name, price)
SELECT * FROM (SELECT '4' AS id, 'Lasagne' AS name, '78' AS price) AS tmp
WHERE NOT EXISTS (
    SELECT name FROM items WHERE id = '4'
) LIMIT 1;
