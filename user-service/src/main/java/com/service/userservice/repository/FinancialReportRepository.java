package com.service.userservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.service.userservice.entity.FinancialReport;

@Repository
public interface FinancialReportRepository extends JpaRepository<FinancialReport, Integer> {

	@Query(value = "from FinancialReport t where t.purchaseDate >= CURDATE()")
	List<FinancialReport> findTodaysPurchases();

	@Query(nativeQuery = true, value = "SELECT * FROM orders")
	List<FinancialReport> fetchAllOrders();
	
	@Query(nativeQuery = true, value = "CALL FetchOrderByPrice(:price)")
	List<FinancialReport> fetchByPrice(int price);
	
	@Query(nativeQuery = true, value = "CALL FetchOrderByDate(:date)")
	List<FinancialReport> fetchByPurchaseDate(String date);
}
